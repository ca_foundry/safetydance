const cards = [
	{
		id: "green-card",
		className: "cardGreen",
		style: "",
        	backgroundUrl: "modules/safety-dance/assets/green.png",
		backgroundColor: "green",
		pauseOnClick: false,
		message: "A green card has been played. It's a good time to check in briefly.",
		alertSound: ""
	},
	{
		id: "yellow-card",
		className: "cardYellow",
		style: "",
        	backgroundUrl: "modules/safety-dance/assets/yellow.png",
		backgroundColor: "yellow",
		pauseOnClick: true,
		message: "A yellow card has been played. Let's pause and discuss.",
		alertSound: ""
	},
	{
		id: "red-card",
		className: "cardRed",
		style: "",
        	backgroundUrl: "modules/safety-dance/assets/red.png",
		backgroundColor: "red",
		pauseOnClick: true,
		message: "A red card has been played. Stop immediately and check in.",
		alertSound: ""
	}
];

let socket;
let activeCard = null;
let cardQueue = [];

const handleCardActivation = (config) => {
	if (activeCard) {
		cardQueue = [...cardQueue, config];
		return;
	}

	activeCard = config;
	const opts = JSON.parse(config);

	const cardEl = document.createElement("div");
	cardEl.style = `position: fixed; top: 25%; left: 50%; height: 50vh; width: 15vw; transform: translateX(-50%); background: url(${opts.backgroundUrl}) ${opts.backgroundColor}; background-size: cover; background-position: center;`;
	document.body.appendChild(cardEl);

	const messageEl = document.createElement("div");
	messageEl.style = "position: fixed; z-index: 500; top: 50%; left: 50%; height: 100px; width: 60vw; font-size: 48pt; color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000; transform: translateX(-50%);";
	messageEl.innerText = opts.message;
	document.body.appendChild(messageEl);

	if (opts.pause == "true") {
		game.togglePause(true);
	}

	if (opts.alertSound) {
		const sound = new URL(opts.alertSound);
		new Audio(sound.href).play();
	}

	setTimeout(() => {
		cardEl.remove();
		messageEl.remove();

		activeCard = null;
		if (cardQueue.length > 0) {
			let [next, ...rest] = cardQueue;
			cardQueue = rest;
			handleCardActivation(next);
		}
	}, 5000);
};

const onCardClick = (ev) => {
	const target = ev.target;
	const payload = JSON.stringify(target.dataset);

	socket.executeForEveryone("handlePayload", payload);
	handleCardActivation(payload);
};

Hooks.on("socketlib.ready", () => {
	socket = socketlib.registerModule("safety-dance");
	socket.register("handlePayload", handleCardActivation);
});

Hooks.on("ready", () => {
    const sidebar = document.getElementById("sidebar");
    const cardContainer = document.createElement("div");
    cardContainer.className = "sdCardContainer";
    sidebar.appendChild(cardContainer);

    cards.forEach((card, idx) => {
	    const left = 100/(cards.length)*(idx);
	    const el = document.createElement("div");
	    el.className = `sdCard ${card.className}`;
	    el.style = `${card.style}; left: ${left}px; background: url(${card.backgroundUrl}) ${card.backgroundColor}; background-size: cover; background-position: center;`;
	    el.id = card.id;
	    el.dataset.alertSound = card.alertSound;
	    el.dataset.message = card.message;
	    el.dataset.pause = card.pauseOnClick;
	    el.dataset.backgroundUrl = card.backgroundUrl;
	    el.dataset.backgroundColor = card.backgroundColor;

	    el.addEventListener("click", onCardClick);

	    cardContainer.appendChild(el);
    });
});
